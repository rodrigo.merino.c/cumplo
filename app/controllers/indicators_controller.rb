class IndicatorsController < ApplicationController
  def index
      values = Values.get_range(params[:date_from], params[:date_to], params[:type_value])
      options = {}
			options[:meta] = { means: Metrics.mean(values),
												 maximums: Metrics.maximums(values),
                         minimums: Metrics.minimums(values) }
      render json: IndicatorsSerializer.new(values, options)
	end 
	
end
