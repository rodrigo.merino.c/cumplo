class TmcsController < ApplicationController
  def index
    tmc = Tmc.get_range(params[:date_from], params[:date_to])
    options = {}
    options[:meta] = { maximums: Metrics.maximums_tmc(tmc) }
    #                    maximums: Metrics.maximums(values),
    #                    minimums: Metrics.minimums(values)
    #                  }
    render json: TmcSerializer.new(tmc, options).serialized_json
	end
end
