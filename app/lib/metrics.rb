class Metrics
  def self.mean(values)
    values.average(:value)
  end

  def self.minimums(values)
    values.minimum(:value)
  end

  def self.maximums(values)
    values.maximum(:value)
  end

  def self.maximums_tmc(values)
    values.group(:type_tmc).maximum(:value)
  end
end
