class Tmc < ApplicationRecord
  def self.get_range(init_date, end_date)
    in_date = Date.parse(init_date)
    en_date = Date.parse(end_date)
    sql = "date_from >= '#{in_date}' and date_from < '#{en_date}' "
    ap sql
    if where(sql).blank?
      url_query = ENV['BASE_URL'] +
                  "/tmc/periodo/#{in_date.year}/#{in_date.month}" \
                  "/#{en_date.year}/#{en_date.month}"\
                  "?apikey=#{ENV['API_SBIF']}&formato=json"
      response = HTTParty.get(url_query)
      ap url_query
      if (response["TMCs"]).present?
        if (response["TMCs"]).present?
          response["TMCs"].each do |tmc|
            create(title: tmc['Titulo'],
                    subtitle: tmc['SubTitulo'],
                    value: tmc['Valor'],
                    date_from: tmc['Fecha'],
                    date_to: tmc['Hasta'],
                    type_tmc: tmc['Tipo'])
          end
        end
      end

    end
    where("date_from >= '#{in_date}' and date_from < '#{en_date}' ")
  end
end

#  .get_range('2019/11/15', '2020/01/30')