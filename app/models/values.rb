# Model class to get and retrieve dollars
class Values < ApplicationRecord
  def self.types
    {
      dolar: 'Dolares',
      uf: 'UFs'
    }
  end
  def self.get_range(init_date, end_date, typeval)
    in_date = Date.parse(init_date)
    en_date = Date.parse(end_date)
      [*in_date..en_date].each do |date_temp|
        if where(value_date: date_temp, type_value: typeval).blank?
          url_query = ENV['BASE_URL'] +
                      "/#{typeval}/#{date_temp.year}/#{date_temp.month}/dias/" \
                      "#{date_temp.day}?apikey=#{ENV['API_SBIF']}&formato=json"
          response = HTTParty.get(url_query)
          if (response[types[typeval.to_sym]]).present?
            create(type_value: typeval,
                   value_date: date_temp,
                   value: response[types[typeval.to_sym]][0]["Valor"])
          end
        end
    end

    Values.where("value_date >= '#{in_date}' and value_date < '#{en_date}' and type_value='#{typeval}'")
  end
end
