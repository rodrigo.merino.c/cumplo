class IndicatorsSerializer
  include FastJsonapi::ObjectSerializer
  attributes :value, :value_date, :type_value
end
