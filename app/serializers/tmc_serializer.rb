class TmcSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :subtitle, :value, :date_from, :date_to, :type_tmc
end

