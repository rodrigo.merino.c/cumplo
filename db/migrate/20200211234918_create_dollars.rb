class CreateDollars < ActiveRecord::Migration[5.1]
  def change
    create_table :values do |t|
      t.date :value_date
      t.float :value
      t.string :type_value

      t.timestamps
    end

    add_index :values, :value_date
    add_index :values, :value
    add_index :values, :type_value
  end
end
