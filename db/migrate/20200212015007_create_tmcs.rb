class CreateTmcs < ActiveRecord::Migration[5.1]
  def change
    create_table :tmcs do |t|
      t.string :title
      t.string :subtitle
      t.float :value
      t.date :date_from
      t.date :date_to
      t.integer :type_tmc

      t.timestamps
    end
  end
end
